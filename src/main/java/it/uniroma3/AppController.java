package it.uniroma3;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class AppController {
 
    @Autowired
    private UserRepository userRepo;
     
    @Autowired
    private UserServices service;
    
    @GetMapping("")
    public String viewHomePage() {
        return "index";
    }
    
    @GetMapping("/login")
    public String loginPage() {
        return "login";
    }
    
    @GetMapping("/register")   
    public String showRegistrationForm(Model model) {
        model.addAttribute("user", new User());
         
        return "signup_form";
    }
    
   
    @PostMapping("/process_register") 
    public String processRegister(User user, Model model, HttpServletRequest request) //gestisce l'invio del modulo di registrazione 
            throws UnsupportedEncodingException, MessagingException {
       
    	service.register(user); 
    	String siteURl = Utility.getSiteURL(request); //restituisce il percorso di contesto reale dell'applicazione web, 
       service.sendVerificationEmail(user, siteURl);									//quindi il collegamento di verifica funzionerà quando l'utente si apre l'email.
        return "register_success";
    }
    
    
    
    @GetMapping("/users")
    public String listUsers(Model model) {
        List<User> listUsers = userRepo.findAll();
        model.addAttribute("listUsers", listUsers);
        return "users";
    }
    
    @GetMapping("/verify")
    public String verifyUser(@Param("code") String code) {   //restituisce il nome della vista in base al risultato della verifica.
        if (service.verify(code)) {
            return "verify_success";
        } else {
            return "verify_fail";
        }
    }
    
   
}